public with sharing class ContactSearch {



    public static List<Contact> searchForContacts(String lastName, String postalCode){

        Contact[] con=[SELECT Name, ID FROM Contact WHERE LastName=:lastName AND MailingPostalCode=:postalCode];   
        return con;

    }
}