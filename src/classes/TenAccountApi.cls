global with sharing class TenAccountApi {

    global with sharing class AccountApiAdvanced{
        webservice String Nam;
        webservice String IDext;
        public AccountApiAdvanced(String n, String I){
            nam=n;
            IDext=I;
        }
    }

    webservice static List<AccountApiAdvanced> giveMeTenAccount( Integer resu){

        List<Account> accTemp=[SELECT Name, ID FROM Account ORDER BY CreatedDate DESC LIMIT :resu];

        List<AccountApiAdvanced> goodList=new List<AccountApiAdvanced>();

        for(Account a: accTemp){
            goodList.add(new AccountApiAdvanced(a.Name,a.ID));
        }
        
        return goodList; 

    }

}