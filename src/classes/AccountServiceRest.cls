@RestResource (urlMapping='/Account')
global with sharing class AccountServiceRest {


    global class ReqDTO{
        webservice String Name; 
        webservice String AccNum;
    }

    global class resDTO{
        webservice String IDAcc;
        webservice String Error;

        private resDTO(String IA, String E){
            IDAcc=IA;
            Error=E;
        }
        private resDTO(){
            IDAcc='';
            Error='';
        }
    }


    @HttpGet
    global static List<Account> getAccountRest(){
        RestRequest param=RestContext.request;
        Integer lim=Integer.valueOf(param.params.get('limitArray'));
        List<Account> resu = [SELECT Name, ID FROM Account ORDER BY CreatedDate DESC LIMIT :lim];

        return resu;
    }

    @HttpPost
    global static void createAccount(){
        
        
        
        String bodystring=RestContext.request.requestBody.toString();
        ReqDTO request=(ReqDTO)JSON.Deserialize(bodystring, ReqDTO.class);
        String answer;
        Account acc=new Account(Name=request.Name, AccountNumber=request.AccNum);
        try{
            insert acc;
            
            answer=JSON.Serialize(acc);
            RestContext.Response.StatusCode=201;
        //    return answer;
        }catch(DMLException e){
            answer=e.getmessage();
            RestContext.Response.StatusCode=400;
            //return answer;
        }catch(Exception e2){
            answer=e2.getmessage();
            RestContext.Response.StatusCode=500;
            //return answer;
        }
        RestContext.Response.Responsebody=Blob.valueOf(answer);
        RestContext.Response.Headers.put('Content-Type','application/json' );

    }
}