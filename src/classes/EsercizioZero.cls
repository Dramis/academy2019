public with sharing class EsercizioZero {

    public static List<String> generateStringArray(Integer n){
        
        String[] superLista=new List<String>();
        
        for(Integer i=0;i<=n;i++){
            superLista.add('Test '+i);
        }

        return superLista;
    }

}