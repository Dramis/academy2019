public with sharing class AccountUtils {

    public static List<Account> accountsByState(String m){
        List<Account> account=[SELECT ID, Name FROM Account WHERE BillingState=:m];
        return account;
    }

}