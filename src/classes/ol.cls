global with sharing class ol {

    global class ReqDTO{
        webservice String Name; 
        webservice String AccNum;
    }

    global class resDTO{
        webservice String IDAcc;
        webservice String Error;

        private resDTO(String IA, String E){
            IDAcc=IA;
            Error=E;
        }
        private resDTO(){
            IDAcc='';
            Error='';
        }
    }

    webservice static resDTO AccCreation(ReqDTO request){
        
        Account temp=new Account(Name=request.Name, AccountNumber=request.AccNum);
        resDTO risul=new resDTO();
        try{
            insert temp;
            risul.IDAcc=temp.ID;
        }catch(Exception e){
            risul.error=e.getmessage();
        } 
        return risul;
        
    }

}