public class provaClasse {
    
    private integer prova;
    
    public provaClasse(integer n){
        prova=n;
    }

    public integer getprova(){
        return prova;
    }

    public integer fattoriale(integer n){
    
        if(n==0){
            return 1;
        }else{
            return n*fattoriale(n-1);
        }
    }

}