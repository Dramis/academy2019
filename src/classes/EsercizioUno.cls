public with sharing class EsercizioUno implements Queueable{

    public List<Double> listone=new List<Double>();

    private static final Integer COSTANTONA=100;

    public void execute(QueueableContext context){

        for(Integer i=0; i<COSTANTONA; i++){
            listone.add(Math.Random());
            System.debug(listone[i]);
        }

    }

}